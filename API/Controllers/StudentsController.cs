using Microsoft.AspNetCore.Mvc;
using API.Domain.Models;
using API.Domain.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using API.Resources;
using API.Extensions;

namespace API.Controllers
{
    [Route("/api/[controller]")]
    public class StudentsController : Controller
    {
        private readonly IStudentService _studentService;
        private readonly IMapper _mapper;

        public StudentsController(IStudentService studentService, IMapper mapper)
        {
            _studentService = studentService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<StudentResource>> GetAllAsync()
        {
            var students = await _studentService.ListAsync();
            var resources = _mapper.Map<IEnumerable<Student>, IEnumerable<StudentResource>>(students);

            return resources;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveStudentResource resource)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var student = _mapper.Map<SaveStudentResource, Student>(resource);
            var result = await _studentService.SaveAsync(student);

            if (!result.Success) {
                return BadRequest(result.Message);
            }

            var studentResource = _mapper.Map<Student, StudentResource>(result.Student);

            return Ok(studentResource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(uint id, [FromBody] SaveStudentResource resource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var student = _mapper.Map<SaveStudentResource, Student>(resource);
            var result = await _studentService.UpdateAsync(id, student);

            if (!result.Success)
                return BadRequest(result.Message);

            var studentResource = _mapper.Map<Student, StudentResource>(result.Student);

            return Ok(studentResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(uint id)
        {
            var result = await _studentService.DeleteAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var studentResource = _mapper.Map<Student, StudentResource>(result.Student);

            return Ok(studentResource);
        }
    }
}