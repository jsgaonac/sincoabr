using Microsoft.AspNetCore.Mvc;
using API.Domain.Models;
using API.Domain.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Resources;
using AutoMapper;
using API.Extensions;

namespace API.Controllers
{
    [Route("/api/[controller]")]
    public class CoursesController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly IMapper _mapper;

        public CoursesController(ICourseService courseService, IMapper mapper)
        {
            _courseService = courseService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<CourseResource>> GetAllAsync()
        {
            var courses = await _courseService.ListAsync();
            var resources = _mapper.Map<IEnumerable<Course>, IEnumerable<CourseResource>>(courses);

            return resources;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveCourseResource resource)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var course = _mapper.Map<SaveCourseResource, Course>(resource);
            var result = await _courseService.SaveAsync(course);

            if (!result.Success) {
                return BadRequest(result.Message);
            }

            var courseResource = _mapper.Map<Course, CourseResource>(result.Course);

            return Ok(courseResource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(uint id, [FromBody] SaveCourseResource resource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var course = _mapper.Map<SaveCourseResource, Course>(resource);
            var result = await _courseService.UpdateAsync(id, course);

            if (!result.Success)
                return BadRequest(result.Message);

            var courseResource = _mapper.Map<Course, CourseResource>(result.Course);

            return Ok(courseResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(uint id)
        {
            var result = await _courseService.DeleteAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var courseResource = _mapper.Map<Course, CourseResource>(result.Course);

            return Ok(courseResource);
        }
    }
}