using Microsoft.AspNetCore.Mvc;
using API.Domain.Models;
using API.Domain.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using API.Resources;
using API.Extensions;

namespace API.Controllers
{
    [Route("/api/[controller]")]
    public class TeachersController : Controller
    {
        private readonly ITeacherService _teacherService;
        private readonly IMapper _mapper;

        public TeachersController(ITeacherService teacherService, IMapper mapper)
        {
            _teacherService = teacherService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<TeacherResource>> GetAllAsync()
        {
            var teachers = await _teacherService.ListAsync();
            var resources = _mapper.Map<IEnumerable<Teacher>, IEnumerable<TeacherResource>>(teachers);

            return resources;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveTeacherResource resource)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var teacher = _mapper.Map<SaveTeacherResource, Teacher>(resource);
            var result = await _teacherService.SaveAsync(teacher);

            if (!result.Success) {
                return BadRequest(result.Message);
            }

            var teacherResource = _mapper.Map<Teacher, TeacherResource>(result.Teacher);

            return Ok(teacherResource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(uint id, [FromBody] SaveTeacherResource resource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var teacher = _mapper.Map<SaveTeacherResource, Teacher>(resource);
            var result = await _teacherService.UpdateAsync(id, teacher);

            if (!result.Success)
                return BadRequest(result.Message);

            var teacherResource = _mapper.Map<Teacher, TeacherResource>(result.Teacher);

            return Ok(teacherResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(uint id)
        {
            var result = await _teacherService.DeleteAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var teacherResource = _mapper.Map<Teacher, TeacherResource>(result.Teacher);

            return Ok(teacherResource);
        }
    }
}