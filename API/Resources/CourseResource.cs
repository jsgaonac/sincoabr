using System;

namespace API.Resources
{
    public class CourseResource
    {
        public uint Id { get; set; }
        public string Name { get; set; }
    }
}