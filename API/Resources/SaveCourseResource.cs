using System;
using System.ComponentModel.DataAnnotations;

namespace API.Resources
{
    public class SaveCourseResource
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}