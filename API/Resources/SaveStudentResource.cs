using System;
using System.ComponentModel.DataAnnotations;

namespace API.Resources
{
    public class SaveStudentResource
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public byte Grade { get; set; }

        [Required]
        [MaxLength(10)]
        public string StudentClass { get; set; }

        [Required]
        public DateTime EnrollmentDate { get; set; }

        [Required]
        [MaxLength(50)]
        public string TutorName { get; set; }

        [Required]
        [MaxLength(70)]
        public string Address { get; set; }

        [Required]
        [MaxLength(12)]
        public string PhoneNumber { get; set; }
    }
}