using System;
using System.ComponentModel.DataAnnotations;

namespace API.Resources
{
    public class SaveTeacherResource
    {
        [Required]
        [MaxLength(70)]
        public string Name { get; set; }

        [Required]
        [MaxLength(70)]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(70)]
        public string Address { get; set; }

        [Required]
        public uint Salary { get; set; }

        [Required]
        public DateTime HiringDate { get; set; }

    }
}