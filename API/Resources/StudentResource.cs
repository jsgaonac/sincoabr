using System;

namespace API.Resources
{
    public class StudentResource
    {
        public uint Id { get; set;}
        public string Name { get; set; }
        public byte Grade { get; set; }
        public string StudentClass { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public string TutorName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}