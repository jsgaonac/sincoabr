using System;

namespace API.Resources
{
    public class TeacherResource
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public uint Salary { get; set; }
        public DateTime HiringDate { get; set; }
    }
}