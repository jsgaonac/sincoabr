using System.Collections.Generic;
using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Services;
using API.Domain.Services.Communication;
using API.Domain.Repositories;
using System;

namespace API.Services
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StudentService(IStudentRepository studentRepository, IUnitOfWork unitOfWork)
        {
            _studentRepository = studentRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Student>> ListAsync()
        {
            return await _studentRepository.ListAsync();
        }

        public async Task<StudentResponse> SaveAsync(Student student)
        {
            try
            {
                await _studentRepository.AddAsync(student);
                await _unitOfWork.CompleteAsync();

                return new StudentResponse(student);
            }
            catch (Exception e)
            {
                return new StudentResponse($"An error ocurred when saving student: {e.Message}");
            }
        }

        public async Task<StudentResponse> UpdateAsync(uint id, Student student)
        {
            var existingStudent = await _studentRepository.FindByIdAsync(id);

            if (existingStudent == null)
            {
                return new StudentResponse("Student not found.");
            }

            existingStudent.Name = student.Name;
            existingStudent.Grade = student.Grade;
            existingStudent.StudentClass = student.StudentClass;
            existingStudent.EnrollmentDate = student.EnrollmentDate;
            existingStudent.TutorName = student.TutorName;
            existingStudent.Address = student.Address;
            existingStudent.PhoneNumber = student.PhoneNumber;

            try
            {
                _studentRepository.Update(existingStudent);
                await _unitOfWork.CompleteAsync();

                return new StudentResponse(existingStudent);
            }
            catch (Exception e)
            {
                return new StudentResponse($"An error ocurred when updating student: {e.Message}");
            }
        }

        public async Task<StudentResponse> DeleteAsync(uint id)
        {
            var existingStudent = await _studentRepository.FindByIdAsync(id);

            if (existingStudent == null)
                return new StudentResponse("Student not found.");

            try
            {
                _studentRepository.Remove(existingStudent);
                await _unitOfWork.CompleteAsync();

                return new StudentResponse(existingStudent);
            }
            catch (Exception ex)
            {
                return new StudentResponse($"An error occurred when deleting the student: {ex.Message}");
            }
        }
    }
}