using System.Collections.Generic;
using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Services;
using API.Domain.Services.Communication;
using API.Domain.Repositories;
using System;

namespace API.Services
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository _courseRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CourseService(ICourseRepository courseRepository, IUnitOfWork unitOfWork)
        {
            _courseRepository = courseRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Course>> ListAsync()
        {
            return await _courseRepository.ListAsync();
        }

        public async Task<CourseResponse> SaveAsync(Course course)
        {
            try
            {
                await _courseRepository.AddAsync(course);
                await _unitOfWork.CompleteAsync();

                return new CourseResponse(course);
            }
            catch (Exception e)
            {
                return new CourseResponse($"An error ocurred when saving course: {e.Message}");
            }
        }

        public async Task<CourseResponse> UpdateAsync(uint id, Course course)
        {
            var existingCourse = await _courseRepository.FindByIdAsync(id);

            if (existingCourse == null)
            {
                return new CourseResponse("Course not found.");
            }

            existingCourse.Name = course.Name;

            try
            {
                _courseRepository.Update(existingCourse);
                await _unitOfWork.CompleteAsync();

                return new CourseResponse(existingCourse);
            }
            catch (Exception e)
            {
                return new CourseResponse($"An error ocurred when updating course: {e.Message}");
            }
        }

        public async Task<CourseResponse> DeleteAsync(uint id)
        {
            var existingCourse = await _courseRepository.FindByIdAsync(id);

            if (existingCourse == null)
                return new CourseResponse("Course not found.");

            try
            {
                _courseRepository.Remove(existingCourse);
                await _unitOfWork.CompleteAsync();

                return new CourseResponse(existingCourse);
            }
            catch (Exception ex)
            {
                return new CourseResponse($"An error occurred when deleting the course: {ex.Message}");
            }
        }
    }
}