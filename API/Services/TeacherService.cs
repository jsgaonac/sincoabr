using System.Collections.Generic;
using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Services;
using API.Domain.Services.Communication;
using API.Domain.Repositories;
using System;

namespace API.Services
{
    public class TeacherService : ITeacherService
    {
        private readonly ITeacherRepository _teacherRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TeacherService(ITeacherRepository teacherRepository, IUnitOfWork unitOfWork)
        {
            _teacherRepository = teacherRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Teacher>> ListAsync()
        {
            return await _teacherRepository.ListAsync();
        }

        public async Task<TeacherResponse> SaveAsync(Teacher teacher)
        {
            try
            {
                await _teacherRepository.AddAsync(teacher);
                await _unitOfWork.CompleteAsync();

                return new TeacherResponse(teacher);
            }
            catch (Exception e)
            {
                return new TeacherResponse($"An error ocurred when saving teacher: {e.Message}");
            }
        }

        public async Task<TeacherResponse> UpdateAsync(uint id, Teacher teacher)
        {
            var existingTeacher = await _teacherRepository.FindByIdAsync(id);

            if (existingTeacher == null)
            {
                return new TeacherResponse("Teacher not found.");
            }

            existingTeacher.Name = teacher.Name;
            existingTeacher.HiringDate = teacher.HiringDate;
            existingTeacher.Salary = teacher.Salary;
            existingTeacher.Address = teacher.Address;
            existingTeacher.PhoneNumber = teacher.PhoneNumber;

            try
            {
                _teacherRepository.Update(existingTeacher);
                await _unitOfWork.CompleteAsync();

                return new TeacherResponse(existingTeacher);
            }
            catch (Exception e)
            {
                return new TeacherResponse($"An error ocurred when updating teacher: {e.Message}");
            }
        }

        public async Task<TeacherResponse> DeleteAsync(uint id)
        {
            var existingTeacher = await _teacherRepository.FindByIdAsync(id);

            if (existingTeacher == null)
                return new TeacherResponse("Teacher not found.");

            try
            {
                _teacherRepository.Remove(existingTeacher);
                await _unitOfWork.CompleteAsync();

                return new TeacherResponse(existingTeacher);
            }
            catch (Exception ex)
            {
                return new TeacherResponse($"An error occurred when deleting the teacher: {ex.Message}");
            }
        }
    }
}