using System.Collections.Generic;
using System.Threading.Tasks;
using API.Domain.Models;

namespace API.Domain.Repositories
{
    public interface ICourseRepository
    {
        Task<IEnumerable<Course>> ListAsync();
        Task AddAsync(Course course);
        Task<Course> FindByIdAsync(uint id);
        void Update(Course course);
        void Remove(Course course);
    }
}