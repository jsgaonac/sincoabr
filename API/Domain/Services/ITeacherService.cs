using System.Collections.Generic;
using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Services.Communication;

namespace API.Domain.Services
{
    public interface ITeacherService
    {
        Task<IEnumerable<Teacher>> ListAsync();
        Task<TeacherResponse> SaveAsync(Teacher teacher);
        Task<TeacherResponse> UpdateAsync(uint id, Teacher teacher);
        Task<TeacherResponse> DeleteAsync(uint id);
    }
}