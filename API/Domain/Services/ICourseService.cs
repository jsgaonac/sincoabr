using System.Collections.Generic;
using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Services.Communication;

namespace API.Domain.Services
{
    public interface ICourseService
    {
        Task<IEnumerable<Course>> ListAsync();
        Task<CourseResponse> SaveAsync(Course course);
        Task<CourseResponse> UpdateAsync(uint id, Course course);
        Task<CourseResponse> DeleteAsync(uint id);
    }
}