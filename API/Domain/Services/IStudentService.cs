using System.Collections.Generic;
using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Services.Communication;

namespace API.Domain.Services
{
    public interface IStudentService
    {
        Task<IEnumerable<Student>> ListAsync();
        Task<StudentResponse> SaveAsync(Student student);
        Task<StudentResponse> UpdateAsync(uint id, Student student);
        Task<StudentResponse> DeleteAsync(uint id);
    }
}