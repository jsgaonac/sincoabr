using API.Domain.Models;

namespace API.Domain.Services.Communication
{
    public class StudentResponse : BaseResponse
    {
        public Student Student { get; private set; }

        private StudentResponse(bool success, string message, Student student) : base(success, message)
        {
            Student = student;
        }

        public StudentResponse(Student student) : this(true, string.Empty, student)
        {}

        public StudentResponse(string message) : this(false, message, null)
        {}
    }
}