using API.Domain.Models;

namespace API.Domain.Services.Communication
{
    public class CourseResponse : BaseResponse
    {
        public Course Course { get; private set; }

        private CourseResponse(bool success, string message, Course course) : base(success, message)
        {
            Course = course;
        }

        public CourseResponse(Course course) : this(true, string.Empty, course)
        {}

        public CourseResponse(string message) : this(false, message, null)
        {}
    }
}