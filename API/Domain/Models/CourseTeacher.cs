namespace API.Domain.Models
{
    public class CourseTeacher
    {
        public uint TeacherId { get; set; }
        public uint CourseId { get; set; }

        public Course Course { get; set; }
        public Teacher Teacher { get; set; }
    }
}