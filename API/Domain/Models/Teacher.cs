using System.Collections.Generic;
using System;

namespace API.Domain.Models
{
    public class Teacher
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public uint Salary { get; set; }
        public DateTime HiringDate { get; set; }
        public IList<CourseTeacher> Courses { get; set; } = new List<CourseTeacher>();
    }
}