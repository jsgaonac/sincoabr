using System.Collections.Generic;
using System;

namespace API.Domain.Models
{
    public class Student
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public Byte Grade { get; set; }
        public string StudentClass { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public string TutorName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public IList<CourseStudent> Courses { get; set; } = new List<CourseStudent>();
    }
}