using System.Collections.Generic;

namespace API.Domain.Models
{
    public class Course
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public IList<CourseStudent> Students { get; set; } = new List<CourseStudent>();
        public IList<CourseTeacher> Teachers { get; set; } = new List<CourseTeacher>();
    }
}