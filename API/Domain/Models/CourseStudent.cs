namespace API.Domain.Models
{
    public class CourseStudent
    {
        public uint CourseId { get; set; }
        public uint StudentId { get; set; }
        public float GradePeriod1 { get; set; }
        public float GradePeriod2 { get; set; }

        public Course Course { get; set; }
        public Student Student { get; set; }
    }
}