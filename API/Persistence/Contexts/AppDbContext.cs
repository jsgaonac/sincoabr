using Microsoft.EntityFrameworkCore;
using API.Domain.Models;

namespace API.Persistence.Contexts
{
    public class AppDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Course> Courses { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Student>().ToTable("Students");
            builder.Entity<Student>().HasKey(s => s.Id);
            builder.Entity<Student>().Property(s => s.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Student>().Property(s => s.Name).IsRequired();
            builder.Entity<Student>().Property(s => s.Grade).IsRequired();
            builder.Entity<Student>().Property(s => s.StudentClass).IsRequired();
            builder.Entity<Student>().Property(s => s.EnrollmentDate).IsRequired();
            builder.Entity<Student>().Property(s => s.TutorName).IsRequired();
            builder.Entity<Student>().Property(s => s.Address).IsRequired();
            builder.Entity<Student>().Property(s => s.PhoneNumber).IsRequired();

            builder.Entity<CourseStudent>()
                .HasKey(cs => new { cs.CourseId, cs.StudentId });

            builder.Entity<CourseStudent>()
                .HasOne(cs => cs.Course)
                .WithMany(c => c.Students)
                .HasForeignKey(cs => cs.CourseId);


            builder.Entity<CourseStudent>()
                .HasOne(cs => cs.Student)
                .WithMany(c => c.Courses)
                .HasForeignKey(cs => cs.StudentId);

            builder.Entity<Teacher>().ToTable("Teachers");
            builder.Entity<Teacher>().HasKey(t => t.Id);
            builder.Entity<Teacher>().Property(t => t.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Teacher>().Property(t => t.Name).IsRequired();
            builder.Entity<Teacher>().Property(t => t.PhoneNumber).IsRequired();
            builder.Entity<Teacher>().Property(t => t.Address).IsRequired();
            builder.Entity<Teacher>().Property(t => t.Salary).IsRequired();
            builder.Entity<Teacher>().Property(t => t.HiringDate).IsRequired();

            builder.Entity<Course>().ToTable("Courses");
            builder.Entity<Course>().HasKey(c => c.Id);
            builder.Entity<Course>().Property(c => c.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Course>().Property(c => c.Name).IsRequired();

            builder.Entity<CourseTeacher>()
                .HasKey(cs => new { cs.CourseId, cs.TeacherId });

            builder.Entity<CourseTeacher>()
                .HasOne(cs => cs.Course)
                .WithMany(c => c.Teachers)
                .HasForeignKey(cs => cs.CourseId);


            builder.Entity<CourseTeacher>()
                .HasOne(cs => cs.Teacher)
                .WithMany(c => c.Courses)
                .HasForeignKey(cs => cs.TeacherId);
        }
    }
}