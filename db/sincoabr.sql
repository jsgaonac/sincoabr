DROP DATABASE IF EXISTS sincoabr;
CREATE DATABASE sincoabr;

USE sincoabr;

CREATE TABLE Students (
    Id integer unsigned auto_increment PRIMARY KEY,
    Name varchar(50) NOT NULL,
    Grade tinyint unsigned NOT NULL,
    StudentClass varchar(10) NOT NULL,
    EnrollmentDate date NOT NULL,
    TutorName varchar(50) NOT NULL,
    Address varchar(70) NOT NULL,
    PhoneNumber varchar(12) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE Teachers (
    Id integer unsigned auto_increment PRIMARY KEY,
    Name varchar(50) NOT NULL,
    PhoneNumber varchar(12) NOT NULL,
    Address varchar(70) NOT NULL,
    Salary integer unsigned NOT NULL,
    HiringDate date NOT NULL
) ENGINE=InnoDB;

CREATE TABLE Courses (
    Id integer unsigned auto_increment PRIMARY KEY,
    Name varchar(70)
) ENGINE=InnoDB;

CREATE TABLE CoursesStudents (
    StudentId integer unsigned,
    CourseId integer unsigned,
    GradePeriod1 numeric(3, 2),
    GradePeriod2 numeric(3, 2),
    PRIMARY KEY (StudentId, CourseId),
    FOREIGN KEY (StudentId) REFERENCES Students (Id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
    FOREIGN KEY (CourseId) REFERENCES Courses (Id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE CoursesTeachers (
    TeacherId integer unsigned,
    CourseId integer unsigned,
    PRIMARY KEY (TeacherId, CourseId),
    FOREIGN KEY (TeacherId) REFERENCES Teachers (Id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
    FOREIGN KEY (CourseId) REFERENCES Courses (Id)
    ON UPDATE CASCADE
    ON DELETE CASCADE 
) ENGINE=InnoDB;